﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace PostgreSQL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "Courses",
                table => new
                {
                    CourseId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy",
                            NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Courses", x => x.CourseId); });

            migrationBuilder.CreateTable(
                "Students",
                table => new
                {
                    StudentId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy",
                            NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Students", x => x.StudentId); });

            migrationBuilder.CreateTable(
                "CourseStudents",
                table => new
                {
                    StudentId = table.Column<int>(nullable: false),
                    CourseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourseStudents", x => new {x.CourseId, x.StudentId});
                    table.ForeignKey(
                        "FK_CourseStudents_Courses_CourseId",
                        x => x.CourseId,
                        "Courses",
                        "CourseId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_CourseStudents_Students_CourseId",
                        x => x.CourseId,
                        "Students",
                        "StudentId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                "Courses",
                new[] {"CourseId", "Name"},
                new object[,]
                {
                    {1, "Course1"},
                    {2, "Course2"},
                    {3, "Course3"},
                    {4, "Course4"},
                    {5, "Course5"}
                });

            migrationBuilder.InsertData(
                "Students",
                new[] {"StudentId", "Name"},
                new object[,]
                {
                    {1, "Name1"},
                    {2, "Name2"},
                    {3, "Name3"},
                    {4, "Name4"},
                    {5, "Name5"}
                });

            migrationBuilder.InsertData(
                "CourseStudents",
                new[] {"CourseId", "StudentId"},
                new object[,]
                {
                    {1, 1},
                    {1, 2},
                    {2, 3},
                    {2, 4},
                    {3, 5}
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "CourseStudents");

            migrationBuilder.DropTable(
                "Courses");

            migrationBuilder.DropTable(
                "Students");
        }
    }
}