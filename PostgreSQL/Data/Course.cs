﻿using System.Collections.Generic;

namespace PostgreSQL.Data
{
    public class Course
    {
        public Course()
        {
            CourseStudents = new HashSet<CourseStudent>();
        }

        public int CourseId { get; set; }
        public string Name { get; set; }

        public ICollection<CourseStudent> CourseStudents { get; set; }

        public override string ToString()
        {
            return $"   ID:{CourseId} Name:{Name}";
        }
    }
}