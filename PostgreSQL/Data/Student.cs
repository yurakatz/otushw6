﻿using System.Collections.Generic;

namespace PostgreSQL.Data
{
    public class Student
    {
        public Student()
        {
            CourseStudents = new HashSet<CourseStudent>();
        }

        public int StudentId { get; set; }
        public string Name { get; set; }

        public ICollection<CourseStudent> CourseStudents { get; set; }

        public override string ToString()
        {
            return $"   ID:{StudentId} Name:{Name}  ";
        }
    }
}