﻿using Microsoft.EntityFrameworkCore;

namespace PostgreSQL.Data
{
    //1. Создать базу данных PostgreSQL для одной из компаний на выбор: Авито, СберБанк, Otus или eBay.Написать скрипт создания 3 таблиц, которые должны иметь первичные ключи и быть соединены внешними ключами.
    //2. Написать скрипт заполнения таблиц данными, минимум по пять строк в каждую.


    internal class ApplicationContext : DbContext
    {
        public ApplicationContext()
        {
            Database.EnsureCreated();
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<CourseStudent> CourseStudents { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(
                "Host=localhost;Port=5432;Database=mytestdb;Username=postgres;Password=mysecretpassword");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CourseStudent>(entity =>
            {
                entity.HasKey(e => new {e.CourseId, e.StudentId});

                entity.HasOne(d => d.course)
                    .WithMany(p => p.CourseStudents)
                    .HasForeignKey(d => d.CourseId);

                entity.HasOne(d => d.student)
                    .WithMany(p => p.CourseStudents)
                    .HasForeignKey(d => d.CourseId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });


            modelBuilder.Entity<Student>().HasData(
                new Student {StudentId = 1, Name = "Name1"},
                new Student {StudentId = 2, Name = "Name2"},
                new Student {StudentId = 3, Name = "Name3"},
                new Student {StudentId = 4, Name = "Name4"},
                new Student {StudentId = 5, Name = "Name5"}
            );
            modelBuilder.Entity<Course>().HasData(
                new Course {CourseId = 1, Name = "Course1"},
                new Course {CourseId = 2, Name = "Course2"},
                new Course {CourseId = 3, Name = "Course3"},
                new Course {CourseId = 4, Name = "Course4"},
                new Course {CourseId = 5, Name = "Course5"}
            );

            modelBuilder.Entity<CourseStudent>().HasData(
                new CourseStudent {CourseId = 1, StudentId = 1},
                new CourseStudent {CourseId = 1, StudentId = 2},
                new CourseStudent {CourseId = 2, StudentId = 3},
                new CourseStudent {CourseId = 2, StudentId = 4},
                new CourseStudent {CourseId = 3, StudentId = 5}
            );
        }
    }
}