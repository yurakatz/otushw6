﻿using System;
using System.Linq;
using PostgreSQL.Data;

namespace PostgreSQL
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var context = new ApplicationContext();

            //3.Создать консольную программу, которая выводит содержимое всех таблиц.
            //4.Добавить в программу возможность добавления в таблицу на выбор.
            char input;


            do
            {
                ShowMenu();
                input = Console.ReadKey().KeyChar;
                Console.WriteLine();
                switch (input)
                {
                    case '1':
                        PrintStudents();
                        break;
                    case '2':
                        PrintCourses();
                        break;
                    case '3':
                        AddCourse();
                        break;
                    case '4':
                        AddStudent();
                        break;
                    case '5':
                        StudentToCurse();
                        break;
                }
            } while (input != 'x');


            void ShowMenu()
            {
                Console.WriteLine();
                Console.WriteLine("1  Print Students Table");
                Console.WriteLine("2  Print Course Table");
                Console.WriteLine("3  Add Course");
                Console.WriteLine("4  Add Student");
                Console.WriteLine("5  Add Student to course");
                Console.WriteLine("x  for Exit");
            }

            void AddCourse()
            {
                Console.WriteLine("Insert Course Name");
                var inputCourseName = Console.ReadLine();
                if (context.Courses.Any(p => p.Name == inputCourseName))
                {
                    Console.WriteLine("Course Already Exist");
                }
                else
                {
                    context.Courses.Add(new Course {Name = inputCourseName});
                    context.SaveChanges();
                }
            }

            void AddStudent()
            {
                Console.WriteLine("Insert Student Name");
                var inputStudentName = Console.ReadLine();
                if (context.Students.Any(p => p.Name == inputStudentName))
                {
                    Console.WriteLine("Student Already Exist");
                }
                else
                {
                    context.Students.Add(new Student {Name = inputStudentName});
                    context.SaveChanges();
                }
            }

            void PrintStudents()
            {
                Console.WriteLine("---------------------students-------------------");
                context.Students.ToList().ForEach(i => Console.WriteLine(i.ToString()));
                Console.WriteLine("------------------------------------------------");
            }

            void PrintCourses()
            {
                Console.WriteLine("---------------------courses-------------------");
                context.Courses.ToList().ForEach(i => Console.WriteLine(i.ToString()));
                Console.WriteLine("------------------------------------------------");
            }

            void StudentToCurse()
            {
                PrintStudents();

                Console.WriteLine("Insert Student ID");

                if (!int.TryParse(Console.ReadLine(), out var studentId) ||
                    !context.Students.Any(p => p.StudentId == studentId))
                {
                    Console.WriteLine("Invalid ID");
                    return;
                }

                PrintCourses();
                Console.WriteLine("Insert Course ID");
                if (!int.TryParse(Console.ReadLine(), out var curseId) ||
                    !context.Courses.Any(p => p.CourseId == studentId))
                {
                    Console.WriteLine("Invalid ID");
                    return;
                }

                if (context.CourseStudents.Any(p => p.CourseId == curseId && p.StudentId == studentId))
                {
                    Console.WriteLine("student already in the course");
                    return;
                }

                context.CourseStudents.Add(new CourseStudent {CourseId = curseId, StudentId = studentId});
                context.SaveChanges();
            }
        }
    }
}